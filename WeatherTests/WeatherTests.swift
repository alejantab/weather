//
//  WeatherTests.swift
//  WeatherTests
//
//  Created by Alejandro Tabares on 27/01/22.
//

import XCTest
@testable import Weather

class WeatherTests: XCTestCase {
    var sut: URLSession!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        
        sut = URLSession(configuration: .default)
    }

    override func tearDownWithError() throws {
        sut = nil
        
        try super.tearDownWithError()
    }

    func testValidApiCall() throws {
        let url = URL(string: Constants.URL.basePath)!
        let promise = expectation(description: "Status code: 401") // Becaue token is empty
        
        let dataTask = sut.dataTask(with: url) { _, response, error in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 401 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        
        dataTask.resume()
        wait(for: [promise], timeout: 5)
    }

    func testPerformanceExample() throws {
        self.measure {
        }
    }
}
