//
//  PermissionPresenter.swift
//  Weather
//
//  Created by Alejandro Tabares on 28/01/22.
//

import UIKit
import CoreLocation

class PermissionPresenter {
    private var view: PermissionView
    
    init(view: PermissionView) {
        self.view = view
    }
    
    func prepareContent() {
        showViews()
    }
    
    func handleAccept() {
        checkLocationPermission()
    }
    
    func handleLocationPermission() {
        UserDefaults.standard.setLocationPermissionChecked(value: true)
        self.view.showNextVC(segueIdentifier: Constants.Segue.cityPicker)
    }
    
    private func showViews() {
        self.view.showTitle()
        self.view.showMessage()
        self.view.showDescriptionImage()
        self.view.showAcceptButton()
    }
    
    private func checkLocationPermission() {
        DispatchQueue.main.async {
            if CLLocationManager.locationServicesEnabled() {
                self.view.requestLocationPermission()
            } else {
                self.view.showAlertInfo(message: "allLocationServiceDenied".localized, completion: {
                    self.handleLocationPermission()
                })
            }
        }
    }
}
