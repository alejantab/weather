//
//  PermissionView.swift
//  Weather
//
//  Created by Alejandro Tabares on 28/01/22.
//

import Foundation

protocol PermissionView: BaseView {
    func showTitle()
    func showMessage()
    func showDescriptionImage()
    func showAcceptButton()
    func requestLocationPermission()
}
