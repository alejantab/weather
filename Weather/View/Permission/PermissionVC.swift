//
//  PermissionVC.swift
//  Weather
//
//  Created by Alejandro Tabares on 28/01/22.
//

import UIKit
import CoreLocation

class PermissionVC: BaseVC {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var descriptionImage: UIImageView!
    @IBOutlet weak var acceptButton: UIButton!
    
    private var presenter: PermissionPresenter!
    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.presenter.prepareContent()
    }
    
    @IBAction func acceptAction(_ sender: UIButton) {
        self.presenter.handleAccept()
    }
}

extension PermissionVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.presenter.handleLocationPermission()
    }
}

extension PermissionVC: PermissionView {
    func showTitle() {
        UIView.animate(withDuration: 1.3, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
            self.titleLabel.alpha = 1
            self.titleLabel.transform = .identity
        })
    }
    
    func showMessage() {
        UIView.animate(withDuration: 1.5, delay: 0.5, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
            self.messageLabel.alpha = 1
            self.messageLabel.transform = .identity
        })
    }
    
    func showDescriptionImage() {
        UIView.animate(withDuration: 1.5, delay: 0.5, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
            self.descriptionImage.alpha = 1
            self.descriptionImage.transform = .identity
        })
    }
    
    func showAcceptButton() {
        UIView.animate(withDuration: 1.3, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.acceptButton.alpha = 1
            self.acceptButton.transform = .identity
        })
    }
    
    func requestLocationPermission() {
        self.locationManager.delegate = self
        
        self.locationManager.requestWhenInUseAuthorization()
    }
}

extension PermissionVC {
    private func initUI() {
        self.presenter = PermissionPresenter(view: self)
        self.titleLabel.alpha = 0
        self.messageLabel.alpha = 0
        self.descriptionImage.alpha = 0
        self.acceptButton.alpha = 0
        self.titleLabel.transform = CGAffineTransform(translationX: 0, y: -50)
        self.messageLabel.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.descriptionImage.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.acceptButton.transform = CGAffineTransform(translationX: 0, y: 250)
    }
}
