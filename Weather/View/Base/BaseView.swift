//
//  BaseView.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import UIKit

protocol BaseView {
    func startProgress()
    func stopProgress()
    func showAlertInfo(message: String, completion: (() -> ())?)
    func showNextVC(segueIdentifier: String)
    func presentVC(vc: UIViewController)
}
