//
//  BaseVC.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import UIKit
import SVProgressHUD

class BaseVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension BaseVC: BaseView {
    func startProgress() {
        SVProgressHUD.show(withStatus: "allWait".localized)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
    }
    
    func stopProgress() {
        SVProgressHUD.dismiss()
    }
    
    func showAlertInfo(message: String, completion: (() -> ())?) {
        let storyboard = UIStoryboard(name: Constants.Storyboard.customAlerts, bundle: nil)
        let infoAlertVC = storyboard.instantiateViewController(withIdentifier: "infoAlertVC") as! InfoAlertVC
        infoAlertVC.infoMessage = message
        infoAlertVC.completion = completion
        
        self.present(infoAlertVC, animated: true, completion: nil)
    }
    
    func showNextVC(segueIdentifier: String) {
        self.performSegue(withIdentifier: segueIdentifier, sender: self)
    }
    
    func presentVC(vc: UIViewController) {
        self.present(vc, animated: true, completion: nil)
    }
}
