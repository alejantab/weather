//
//  WeatherCollectionVCell.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import UIKit

class WeatherCollectionVCell: UICollectionViewCell {
    static let cellIdentifier = "weatherCellIdentifier"
    
    @IBOutlet weak var contentCardView: CardView!
    @IBOutlet weak var propertyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    func loadContent(data: PropertyValue, cardColor: (color1: UIColor, color2: UIColor)) {
        setupBackground(cardColor: cardColor)
        
        self.propertyLabel.text = data.property
        self.valueLabel.text = data.value
    }
    
    private func setupBackground(cardColor: (color1: UIColor, color2: UIColor)) {
        self.contentCardView.horizontalGradient = true
        self.contentCardView.firstColor = cardColor.color1
        self.contentCardView.secondColor = cardColor.color2
    }
}
