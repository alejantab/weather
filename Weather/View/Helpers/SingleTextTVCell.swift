//
//  SingleTextTVCell.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import UIKit

class SingleTextTVCell: UITableViewCell {
    static let cellIdentifier = "singleTextCellIdentifier"
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func loadContent(description: String) {
        self.descriptionLabel.text = description
    }
}
