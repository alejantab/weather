//
//  SplashVC.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import UIKit

class SplashVC: BaseVC {
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    
    private var presenter: SplashPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }
    
    override var prefersStatusBarHidden: Bool {
      return true
    }
}

extension SplashVC: SplashView {
    func showBackgroundImage() {
        UIView.animate(withDuration: 1.0) {
            self.backgroundImageView.alpha = 0.5
        }
    }
    
    func hideAppName() {
        UIView.animate(withDuration: 0.7) {
            self.appNameLabel.alpha = 0
            self.appNameLabel.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }
    }
}

extension SplashVC {
    func initUI() {
        self.presenter = SplashPresenter(view: self)
        self.backgroundImageView.alpha = 0
        
        self.presenter.prepareContent()
    }
}
