//
//  SplashPresenter.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import UIKit

class SplashPresenter {
    private var view: SplashView
    
    init(view: SplashView) {
        self.view = view
    }
    
    func prepareContent() {
        self.view.showBackgroundImage()
        showNextVC()
    }
    
    private func showNextVC() {
        delay(0.5) {
            self.view.hideAppName()
        }
        
        delay(1) {
            if UserDefaults.standard.isLocationPermissionChecked() {
                self.view.showNextVC(segueIdentifier: Constants.Segue.cityPicker)
            } else {
                self.view.showNextVC(segueIdentifier: Constants.Segue.permission)
            }
        }
    }
}
