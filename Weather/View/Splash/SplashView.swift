//
//  SplashView.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

protocol SplashView: BaseView {
    func showBackgroundImage()
    func hideAppName()
}
