//
//  WeatherDetailVC.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import UIKit

class WeatherDetailVC: BaseVC {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var dataCollectionView: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    
    var weather: WeatherRS!
    
    private var presenter: WeatherDetailPresenter!
    private var weatherDataList = [PropertyValue]()
    private var backgroundColors = [(color1: UIColor, color2: UIColor)]()
    private var colorIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        showCityName()
        showTemp()
        showData()
        showBackButton()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}

extension WeatherDetailVC: WeatherDetailView {
    func setCityName(name: String) {
        self.cityNameLabel.text = name
    }
    
    func setTemp(temp: String) {
        self.tempLabel.text = temp
    }
    
    func showCityName() {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
            self.cityNameLabel.alpha = 1
            self.cityNameLabel.transform = .identity
        })
    }
    
    func showTemp() {
        UIView.animate(withDuration: 0.7, delay: 0.5, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
            self.tempLabel.alpha = 1
            self.tempLabel.transform = .identity
        })
    }
    
    func showData() {
        UIView.animate(withDuration: 0.7, delay: 0.5, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
            self.dataCollectionView.alpha = 1
            self.dataCollectionView.transform = .identity
        })
    }
    
    func showBackButton() {
        UIView.animate(withDuration: 1.3, delay: 0.2, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.backButton.alpha = 1
            self.backButton.transform = .identity
        })
    }
    
    func loadContent(dataList: [PropertyValue]) {
        self.weatherDataList = dataList
        
        self.dataCollectionView.reloadData()
    }
}

extension WeatherDetailVC: AppGridLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension WeatherDetailVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.weatherDataList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WeatherCollectionVCell.cellIdentifier, for: indexPath) as! WeatherCollectionVCell
        cell.loadContent(data: self.weatherDataList[indexPath.item], cardColor: self.backgroundColors[self.colorIndex])
        
        if self.colorIndex < backgroundColors.count - 1 {
            self.colorIndex = self.colorIndex + 1
        } else {
            self.colorIndex = 0
        }
        
        return cell
    }
}

extension WeatherDetailVC {
    private func initUI() {
        prepareCardColors()
        
        self.presenter = WeatherDetailPresenter(view: self)
        self.cityNameLabel.alpha = 0
        self.tempLabel.alpha = 0
        self.dataCollectionView.alpha = 0
        self.backButton.alpha = 0
        self.cityNameLabel.transform = CGAffineTransform(translationX: 0, y: -50)
        self.tempLabel.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.dataCollectionView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.backButton.transform = CGAffineTransform(translationX: 0, y: 250)
        self.dataCollectionView.dataSource = self
        
        if let layout = self.dataCollectionView?.collectionViewLayout as? AppGridLayout {
            layout.delegate = self
        }
        
        self.presenter.prepareContent(weather: self.weather)
    }
    
    private func prepareCardColors() {
        self.backgroundColors.append((color1: #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1), color2: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)))
        self.backgroundColors.append((color1: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), color2: #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)))
        self.backgroundColors.append((color1: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), color2: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)))
        self.backgroundColors.append((color1: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), color2: #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)))
    }
}
