//
//  WeatherDetailView.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

protocol WeatherDetailView: BaseView {
    func setCityName(name: String)
    func setTemp(temp: String)
    func showCityName()
    func showTemp()
    func showData()
    func showBackButton()
    func loadContent(dataList: [PropertyValue])
}
