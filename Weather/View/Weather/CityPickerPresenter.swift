//
//  CityPickerPresenter.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import Foundation
import RxSwift
import CoreLocation

class CityPickerPresenter: NSObject {
    private var view: CityPickerView
    private var cityList = [String]()
    private let disposeBag = DisposeBag()
    
    init(view: CityPickerView) {
        self.view = view
    }
    
    func prepareContent() {
        prepareCities()
    }
    
    func handleSeachByName(filter: String) {
        let filteredList = cityList.filter {
            $0.lowercased().range(of: filter, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        if filteredList.isEmpty {
            self.view.loadCities(cityList: self.cityList, isFiltering: true)
        } else {
            self.view.loadCities(cityList: filteredList, isFiltering: true)
        }
    }
    
    func handleByLocation() {
        if isLocationGranted() {
            fetchWeatherByLocation()
        } else {
            self.view.showAlertInfo(message: "allLocationDenied".localized) {
                self.handleSettings()
            }
        }
    }
    
    func fetchWeatherByCity(cityName: String) {
        if cityName.isEmpty {
            self.view.showAlertInfo(message: "cityPickerEmptyCity".localized, completion: nil)
        }
        
        if NetworkUtils.shared.isReachability {
            self.view.startProgress()
            
            let parameters = [Constants.RestKey.q: cityName, Constants.RestKey.appId: Constants.Setting.weatherApiToken]
            
            WeatherDataManager().fetchByCity(parameters: parameters)
                .observe(on: MainScheduler.instance)
                .subscribe{ event in
                    switch event {
                    case .next(let restResponse):
                        self.view.prepareDetail(detail: restResponse)
                        self.view.showNextVC(segueIdentifier: Constants.Segue.detail)
                    case .error:
                        self.view.showAlertInfo(message: "allApiError".localized, completion: nil)
                        self.view.stopProgress()
                    case .completed:
                        self.view.stopProgress()
                    }
                }
                .disposed(by: self.disposeBag)
        } else {
            self.view.showAlertInfo(message: "allOffline".localized, completion: nil)
        }
    }
    
    private func prepareCities() {
        self.cityList = Constants.Setting.cityList.components(separatedBy: ",")
        self.cityList = cityList.sorted(by: { $0.lowercased() < $1.lowercased() })
        
        delay(1.5) {
            self.view.loadCities(cityList: self.cityList, isFiltering: false)
        }
    }
    
    private func fetchWeatherByLocation() {
        if NetworkUtils.shared.isReachability {
            self.view.startProgress()
            
            let userLocation = LocationUtils.getCurrentLocation()
            let parameters = [Constants.RestKey.latitude: String(userLocation.latitude), Constants.RestKey.longitude: String(userLocation.longitude), Constants.RestKey.appId: Constants.Setting.weatherApiToken]
            
            WeatherDataManager().fetchByCity(parameters: parameters)
                .observe(on: MainScheduler.instance)
                .subscribe{ event in
                    switch event {
                    case .next(let restResponse):
                        self.view.prepareDetail(detail: restResponse)
                        self.view.showNextVC(segueIdentifier: Constants.Segue.detail)
                    case .error:
                        self.view.showAlertInfo(message: "allApiError".localized, completion: nil)
                        self.view.stopProgress()
                    case .completed:
                        self.view.stopProgress()
                    }
                }
                .disposed(by: self.disposeBag)
        } else {
            self.view.showAlertInfo(message: "allOffline".localized, completion: nil)
        }
    }
    
    private func isLocationGranted() -> Bool {
        let authStatus = CLLocationManager.authorizationStatus()
        
        return authStatus == .authorizedWhenInUse || authStatus == .authorizedAlways
    }
    
    private func handleSettings() {
        let settingsURL = URL(string: UIApplication.openSettingsURLString)!
        
        UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
    }
}

extension CityPickerPresenter: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        handleByLocation()
    }
}
