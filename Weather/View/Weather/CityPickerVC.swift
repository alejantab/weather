//
//  CityPickerVC.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import UIKit

class CityPickerVC: BaseVC {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var firstOptionLabel: UILabel!
    @IBOutlet weak var secondOptionLabel: UILabel!
    @IBOutlet weak var byLocationButton: UIButton!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var dataTableView: UITableView!
    
    private var presenter: CityPickerPresenter!
    private var cityList = [String]()
    private var selectedCity = ""
    private var weather: WeatherRS!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        showTitle()
        showFirstOption()
        showSecondOption()
        showByLocationButton()
        showCityTextField()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? WeatherDetailVC {
            vc.weather = self.weather
        }
    }
    
    @IBAction func cityNameChanged(_ sender: UITextField) {
        self.presenter.handleSeachByName(filter: sender.text!)
    }
    
    @IBAction func byLocationAction(_ sender: UIButton) {
        self.presenter.handleByLocation()
    }
}

extension CityPickerVC: CityPickerView {
    func showTitle() {
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: [], animations: {
            self.titleLabel.alpha = 1
            self.titleLabel.transform = .identity
        })
    }
    
    func showFirstOption() {
        UIView.animate(withDuration: 1.5, delay: 0.3, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
            self.firstOptionLabel.alpha = 1
            self.firstOptionLabel.transform = .identity
        })
    }
    
    func showSecondOption() {
        UIView.animate(withDuration: 1.5, delay: 0.8, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
            self.secondOptionLabel.alpha = 1
            self.secondOptionLabel.transform = .identity
        })
    }
    
    func showByLocationButton() {
        UIView.animate(withDuration: 1.5, delay: 0.5, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [.allowUserInteraction], animations: {
            self.byLocationButton.alpha = 1
            self.byLocationButton.transform = .identity
        })
    }
    
    func showCityTextField() {
        UIView.animate(withDuration: 1.5, delay: 1.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0, options: [], animations: {
            self.cityTextField.alpha = 1
            self.cityTextField.transform = .identity
        })
    }
    
    func loadCities(cityList: [String], isFiltering: Bool) {
        self.cityList = cityList
        
        self.dataTableView.reloadData()
        
        if !isFiltering {
            self.dataTableView.enterCellsRightToLeft(width: view.frame.width)
        }
    }
    
    func prepareDetail(detail: WeatherRS) {
        self.weather = detail
    }
}

extension CityPickerVC: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cityList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SingleTextTVCell.cellIdentifier, for: indexPath) as! SingleTextTVCell
        
        cell.loadContent(description: self.cityList[indexPath.row])
        
        return cell
    }
}

extension CityPickerVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCity = self.cityList[indexPath.row]
        
        self.dataTableView.deselectSelectedRow(animated: true)
        self.presenter.fetchWeatherByCity(cityName: self.selectedCity)
    }
}

extension CityPickerVC {
    private func initUI() {
        hideKeyboardWhenTappedAround()
        
        self.presenter = CityPickerPresenter(view: self)
        self.titleLabel.alpha = 0
        self.firstOptionLabel.alpha = 0
        self.secondOptionLabel.alpha = 0
        self.byLocationButton.alpha = 0
        self.cityTextField.alpha = 0
        self.titleLabel.transform = CGAffineTransform(translationX: 0, y: -50)
        self.firstOptionLabel.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.secondOptionLabel.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.byLocationButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.cityTextField.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.dataTableView.dataSource = self
        self.dataTableView.delegate = self
        
        self.presenter.prepareContent()
    }
}
