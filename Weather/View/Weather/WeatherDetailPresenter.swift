//
//  WeatherDetailPresenter.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

class WeatherDetailPresenter {
    private var view: WeatherDetailView
    
    init(view: WeatherDetailView) {
        self.view = view
    }
    
    func prepareContent(weather: WeatherRS) {
        self.view.setCityName(name: weather.cityName)
        buildData(weather: weather)
    }
    
    private func buildData(weather: WeatherRS) {
        var dataList = [PropertyValue]()
        let temp = weather.main.temp - 273.15
        let tempAux = String(format: "%.0f", temp)
        
        self.view.setTemp(temp: String(format: "weatherDetailTemp".localized, tempAux))
        
        for aux in weather.weather {
            dataList.append(PropertyValue(property: aux.main, value: aux.description))
        }
        
        dataList.append(PropertyValue(property: "weatherDetailWindSpeed".localized, value: String(format: "weatherDetailWindSpeedValue".localized, String(weather.wind.speed))))
        dataList.append(PropertyValue(property: "weatherDetailLongitude".localized, value: String(weather.coord.longitude)))
        dataList.append(PropertyValue(property: "weatherDetailLatitude".localized, value: String(weather.coord.latitude)))
        dataList.append(PropertyValue(property: "weatherDetailHumidity".localized, value: String(format: "weatherDetailHumidityValue".localized, String(weather.main.humidity))))
        
        self.view.loadContent(dataList: dataList)
    }
}
