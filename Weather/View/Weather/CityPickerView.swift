//
//  CityPickerView.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

protocol CityPickerView: BaseView {
    func showTitle()
    func showFirstOption()
    func showSecondOption()
    func showByLocationButton()
    func showCityTextField()
    func loadCities(cityList: [String], isFiltering: Bool)
    func prepareDetail(detail: WeatherRS)
}
