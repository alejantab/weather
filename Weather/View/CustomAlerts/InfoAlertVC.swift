//
//  InfoAlertVC.swift
//  Weather
//
//  Created by Alejandro Tabares on 28/01/22.
//

import UIKit

class InfoAlertVC: BaseVC {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var popupView: ViewX!
    @IBOutlet weak var infoLabel: UILabel!
    
    var infoMessage: String!
    var completion: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showContent()
    }
    
    @IBAction func handleAccept(_ sender: UIButton) {
        dismiss(animated: true, completion: {
            DispatchQueue.main.async {
                self.completion?()
            }
        })
    }
}

extension InfoAlertVC {
    func initUI() {
        self.titleLabel.text = "allAppName".localized
        self.infoLabel.text = infoMessage
        self.popupView.alpha = 0
        self.popupView.transform = CGAffineTransform(scaleX: 0.3, y: 2)
    }
    
    private func showContent() {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: .allowUserInteraction, animations: {
            self.popupView.transform = .identity
        })
        
        self.popupView.alpha = 1
    }
}
