//
//  AppDelegate.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupNetworkUtils()
        
        return true
    }
}

extension AppDelegate {
    private func setupNetworkUtils() {
        NetworkUtils.shared.startRechability()
    }
}

