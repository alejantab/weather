//
//  TableViewX.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import UIKit

@IBDesignable
extension UITableView {
    
    @IBInspectable var hideEmptyRows: Bool {
        get {
            return self.hideEmptyRows
        }
        set (hideEmpty) {
            if hideEmpty {
                self.tableFooterView = UIView()
            }
        }
    }
    
    func enterCellsRightToLeft(width: CGFloat) {
        let cells = self.visibleCells
        var delay: TimeInterval = 0.1
        
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: width, y: 0)
        }
        
        for cell in cells {
            UIView.animate(withDuration: 0.7, delay: delay, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [.allowUserInteraction], animations: {
                cell.transform = .identity
            })
            
            delay += 0.15
        }
    }
    
    func deselectSelectedRow(animated: Bool) {
        if let indexPathForSelectedRow = self.indexPathForSelectedRow {
            self.deselectRow(at: indexPathForSelectedRow, animated: animated)
        }
    }
}
