//
//  DataUtils.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import Foundation

extension Data {
    func toString() -> String {
        if let newData = String(data: self, encoding: .utf8) {
            return newData
        } else {
            return ""
        }
    }
}
