//
//  UserDefaultsUtils.swift
//  Weather
//
//  Created by Alejandro Tabares on 28/01/22.
//

import Foundation

extension UserDefaults {
    
    func setLocationPermissionChecked(value: Bool) {
        UserDefaults.standard.set(value, forKey: Constants.DefaultsKey.locationChecked)
    }
    
    func isLocationPermissionChecked() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.DefaultsKey.locationChecked)
    }
    
    func setLocationServiceChecked(value: Bool) {
        UserDefaults.standard.set(value, forKey: Constants.DefaultsKey.locationServiceChecked)
    }
    
    func isLocationServiceChecked() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.DefaultsKey.locationServiceChecked)
    }
}
