//
//  Constants.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

struct Constants {
    
    struct URL {
        static let basePath: String = "https://api.openweathermap.org/data/2.5/weather"
    }
    
    struct Setting {
        static let weatherApiToken: String = "cd1d4d094af3ca99d737ecdb9c7f844e"
        static let cityList: String = "Leticia,Medellín,Arauca,Barranquilla,Bogotá,Cartagena,Tunja,Manizales,Florencia,Yopal,Popayán,Valledupar,Quibdó,Montería,Inírida,San José del Guaviare,Neiva,Riohacha,Santa Marta,Villavicencio,Pasto,Cúcuta,Mocoa,Armenia,Pereira,San Andrés,Bucaramanga,Sincelejo,Ibagué,Cali,Mitú,Puerto Carreño"
    }
    
    struct DefaultsKey {
        static let locationChecked = "locationChecked"
        static let locationServiceChecked = "locationServiceChecked"
    }
    
    struct Storyboard {
        static let customAlerts = "CustomAlerts"
    }
    
    struct Segue {
        static let permission: String = "permissionSegue"
        static let cityPicker: String = "cityPickerSegue"
        static let detail: String = "detailSegue"
    }
    
    struct RestKey {
        static let appId: String = "appid"
        static let q: String = "q"
        static let latitude: String = "lat"
        static let longitude: String = "lon"
    }
}
