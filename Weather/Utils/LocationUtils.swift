//
//  LocationUtil.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import Foundation
import CoreLocation

struct LocationUtils {
    
    static func getCurrentLocation() -> UserLocation {
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        var userLocation = UserLocation()
        
        if let latitude = locationManager.location?.coordinate.latitude, let longitude = locationManager.location?.coordinate.longitude {
            userLocation.latitude = latitude
            userLocation.longitude = longitude
        }
        
        locationManager.stopUpdatingLocation()
        
        return userLocation
    }
}
