//
//  NetworkUtils.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import Reachability

class NetworkUtils: NSObject {
    static let shared = NetworkUtils()
    
    private let reachability = try! Reachability()
    private var _isReachability: Bool = false
    
    var isReachability: Bool {
        get {
            return _isReachability
        }
    }
    
    func startRechability() {
        reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                self._isReachability = true
                
                if reachability.connection == .wifi {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
            }
        }
        
        reachability.whenUnreachable = { reachability in
            self._isReachability = false
            
            print("Not reachable")
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func stopRechability() {
        reachability.stopNotifier()
    }
}
