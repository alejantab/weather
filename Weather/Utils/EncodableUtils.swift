//
//  EncodableUtils.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import Foundation

extension Encodable {
    
    func toJson() -> String {
        do {
            let jsonParams = try JSONEncoder().encode(self)
            let jsonString = String(data: jsonParams, encoding: String.Encoding.utf8) as String?
            
            return jsonString!.trim()
        } catch {
            print(error)
            
            return ""
        }
    }
}
