//
//  StringUtils.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

import UIKit

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
