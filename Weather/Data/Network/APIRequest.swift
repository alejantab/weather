//
//  APIRequest.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import UIKit

public enum RequestType: String {
    case GET, POST
}

protocol APIRequest {
    associatedtype T : Encodable
    var method: RequestType { get }
    var path: String { get }
    var parameters: T { get }
    var data: Data { get }
    var dataName: String { get }
    var mimeType: String { get set }
}

extension APIRequest {
    var data: Data {
        get { return Data() } set {}
    }
    var dataName: String {
        get { return "" } set {}
    }
    var mimeType: String {
        get { return "" } set {}
    }
    
    func request() -> URLRequest {
        if method == RequestType.POST {
            let baseURL = URL(string: Constants.URL.basePath)!
            guard let components = URLComponents(url: baseURL.appendingPathComponent(path), resolvingAgainstBaseURL: false) else {
                fatalError("Unable to create URL components")
            }
            
            guard let url = components.url else {
                fatalError("Could not get url")
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            print("URL -> \(url)\nJson -> \(parameters.toJson())")
            
            do {
                let jsonParams = try JSONEncoder().encode(parameters.self)
                request.httpBody = jsonParams
            } catch {
                print(error)
            }
            
            return request
        } else {
            // GET
            let baseURL: URL = URL(string: Constants.URL.basePath)!
            let params: Dictionary = parameters.self as! [String: String]
            
            guard var components = URLComponents(url: baseURL.appendingPathComponent(path), resolvingAgainstBaseURL: false) else {
                fatalError("Unable to create URL components")
            }
            
            if !params.isEmpty {
                components.queryItems = params.map {
                    URLQueryItem(name: $0, value: $1)
                }
            }
            
            guard let url = components.url else {
                fatalError("Could not get url")
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = method.rawValue
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            return request
        }
    }
}
