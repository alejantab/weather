//
//  APIRest.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import UIKit
import RxSwift

class APIRest {
    
    func makeConnection<T: Decodable>(t: T.Type, request: URLRequest) -> Observable<T> {
        return Observable<T>.create { observer in
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    print("Response -> \(data?.toString() ?? "")")
                    
                    let model: T = try JSONDecoder().decode(T.self, from: data ?? Data())
                    
                    observer.onNext(model)
                } catch let error {
                    observer.onError(error)
                }
                
                observer.onCompleted()
            }
            
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
}
