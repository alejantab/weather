//
//  WeatherRS.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import Foundation

struct WeatherRS: Decodable {
    var coord: Coordinates
    var weather: [Weather]
    var wind: Wind
    var main: Main
    var code: Int
    var cityName: String
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            self.coord = try values.decode(Coordinates.self, forKey: .coord)
        } catch {
            self.coord = Coordinates(longitude: 0, latitude: 0)
        }
        
        do {
            self.weather = try values.decode([Weather].self, forKey: .weather)
        } catch {
            self.weather = [Weather]()
        }
        
        do {
            self.wind = try values.decode(Wind.self, forKey: .wind)
        } catch {
            self.wind = Wind(speed: 0, deg: 0)
        }
        
        do {
            self.main = try values.decode(Main.self, forKey: .main)
        } catch {
            self.main = Main(temp: 0, humidity: 0)
        }
        
        do {
            self.code = try values.decode(Int.self, forKey: .code)
        } catch {
            self.code = 0
        }
        
        do {
            self.cityName = try values.decode(String.self, forKey: .cityName)
        } catch {
            self.cityName = ""
        }
    }

    enum CodingKeys: String, CodingKey {
        case coord
        case weather
        case wind
        case main
        case code = "cod"
        case cityName = "name"
    }
}

struct Coordinates: Decodable {
    var longitude: Double
    var latitude: Double

    enum CodingKeys: String, CodingKey {
        case longitude = "lon"
        case latitude = "lat"
    }
}

struct Weather: Decodable {
    var main: String
    var description: String
    var icon: String

    enum CodingKeys: String, CodingKey {
        case main
        case description
        case icon
    }
}

struct Wind: Decodable {
    var speed: Double
    var deg: Int

    enum CodingKeys: String, CodingKey {
        case speed
        case deg
    }
}

struct Main: Decodable {
    var temp: Double
    var humidity: Double

    enum CodingKeys: String, CodingKey {
        case temp
        case humidity
    }
}
