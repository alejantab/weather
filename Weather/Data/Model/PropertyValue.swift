//
//  PropertyValue.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

struct PropertyValue {
    var property: String
    var value: String
}
