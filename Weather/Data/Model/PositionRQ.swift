//
//  PositionRQ.swift
//  Weather
//
//  Created by Alejandro Tabares on 27/01/22.
//

struct UserLocation {
    var latitude: Double
    var longitude: Double
    
    init() {
        latitude =  0
        longitude =  0
    }
}
