//
//  WeatherRequest.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import RxSwift

class WeatherDataManager {
    private let apiRest = APIRest()
    
    func fetchByCity(parameters: [String: String]) -> Observable<WeatherRS> {
        let apiRequest = FetchByCityRequest(parameters: parameters)
        
        return self.apiRest.makeConnection(t: WeatherRS.self, request: apiRequest.request())
    }
}
