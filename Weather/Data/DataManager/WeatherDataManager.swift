//
//  WeatherDataManager.swift
//  Weather
//
//  Created by Alejandro Tabares on 29/01/22.
//

import Foundation

struct FetchByCityRequest: APIRequest {
    var method = RequestType.GET
    var path = ""
    var parameters = [String: String]()
    
    init(parameters: [String: String]) {
        self.parameters = parameters
    }
}
